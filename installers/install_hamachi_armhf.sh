#!/bin/bash -x

version="2.1.0.198-1"
email=""

apt-get update

apt-get install lsb lsb-core -y

wget "https://www.vpn.net/installers/logmein-hamachi_${version}_armhf.deb"

dpkg -i "logmein-hamachi_${version}_armhf.deb"

# Ensures that hamachi service is running
sleep 5

hamachi login

hamachi set-nick "$(hostname)"

# Prevents being rate limited
sleep 5

hamachi attach ${email}

update-rc.d logmein-hamachi defaults