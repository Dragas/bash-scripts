# Installers

These are general installation scripts to install software that requires multiple steps.

## Explanation

### `install_hamachi_armhf.sh`

Installs hamachi on current raspbian system. Edit the script to add email to which it will be attached so that you would be able to remotely control which network it belongs.

### `install_kura_with_networking.sh`

Follows all the installation steps provided by eclipse kura to install Kura with Network Admin.

Note that DHCPCD and Networking services are required for hamachi to run properly. 

### `install_kura_with_no_networking.sh`

Installs kura, but without network admin. In turn doesn't remove DHCPCD service as well as networking.

### `install_docker_on_ubuntu.sh`

Installs docker according to its instructions on ubuntu server. Runs in `-xe` mode.