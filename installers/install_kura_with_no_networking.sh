#!/bin/bash -x

version="3.2.0"

apt-get update
apt-get install gdebi -y
# apt-get purge dhcpcd5 -y
# apt-get remove network-manager -y
# systemctl disable networking
# rfkill unblock all
command -v java > /dev/null 2>&1 || { echo >&2 "Java not found. Installing openjdk8jre headless"; apt-get install openjdk-8-jre-headless -y; }
wget "http://download.eclipse.org/kura/releases/${version}/kura_${version}_raspberry-pi-2-3-nn_installer.deb"
gdebi "kura_${version}_raspberry-pi-2-3-nn_installer.deb"

echo "reboot your machine now"
#reboot
