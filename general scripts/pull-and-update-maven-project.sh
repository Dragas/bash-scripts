#!/bin/bash -x

git fetch

git pull

# Executes codehaus appassembler plugin to generate executable files for current maven project using the maven wrapper
# if necessary add these as <plugin>
# <groupId>org.codehaus.mojo</groupId>
# <artifactId>appassembler-maven-plugin</artifactId>
# <version>1.10</version>
./mvnw clean package appassembler:assemble