#!/bin/bash -x

service=$1

# Finds $1 .service files and copies them to "user installed" systemd folder
find . -name "${service}" -type f -exec cp "{}" /etc/systemd/system/

#sudo cp ./microphone-stream.service /etc/systemd/system/

# Reloads systemd daemon
systemctl daemon-reload

# Enables the copied service
systemctl enable ${service}

# Starts the copied service
systemctl start ${service}