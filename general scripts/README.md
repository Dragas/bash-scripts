# General Scripts

These scripts do not fall under any particular category.

## Explanations

### `install-systemd.sh`

```
sudo ./install-systemd.sh [service name]
```

Copies provided service (with `.service` extension) to /etc/systemd/system and restarts systemd to run it.

### `pull-and-update-maven-project.sh`

Pulls the current git repository and updates it. Then executes compilation as well as produces binaries with `org.codehaus.mojo:appassembler-maven-plugin:1.10`

Depends on presence of maven wrapper in current repository