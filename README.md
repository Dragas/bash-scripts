# Bash scripts

General bash scripts that I sometimes use

Most of these are meant to be used on raspbian systems, but they can be adapted to work on other systems as well. The main draw back is that most of them depend on either apt or maven.
 
Any criticism and suggestions on how to improve them are welcome.

Should any more scripts be added, their usage should be explained in respective readme.md file. In addition, all scripts must run with `-x` argument, which logs what command is being executed.

Any scripts provided here should follow these guidelines:

* Be put into particular folder if it falls under that category (for example `/installers`)
* That folder must have a `README.md` file.
 
  * If it doesn't, it must be created.
  * If it does, an `Explanation` for added script must be added to that readme. See the structure below.
  * The file must follow the following structure:
  
```markdown
# Folder name (also category name)

[category description]

## Usage


\```
[commandline to run the main script in this package]
\```

[explanation what the main script does and how does it depend on other scripts in this directory]

## Explanations

### [Script name]

\```
[commandline to run the script if necessary]
\```

[explanation what the script does]

```

