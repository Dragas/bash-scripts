#!/bin/bash -x

dp=$1
./uninstall.sh ${dp}
./clean.sh
dp=$(find . -name "*${dp}*.dp")
length=${#dp}
if(("$length" > 0))
then
    ./install.sh ${dp}
else
    echo "Expected dp to be produced. got '${dp}'"
fi