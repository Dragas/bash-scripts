#!/bin/bash -x

. ./constants.sh

token=$(curl \
    -v \
    -b cookies.txt \
    -c cookies.txt \
    -H "Host: ${ip}" \
    -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0" \
    -H "Accept: */*" \
    -H "Accept-Language: en-US,en;q=0.5" \
    -H "Accept-Encoding: gzip, deflate" \
    -H "Referer: http://${ip}/kura" \
    -H "Content-Type: text/x-gwt-rpc; charset=utf-8" \
    -H "X-GWT-Permutation: ${permutation}" \
    -H "X-GWT-Module-Base: http://${ip}/denali/" \
    -H "Authorization: Basic YWRtaW46YWRtaW4=" \
    -H "Upgrade-Insecure-Requests: 1" \
    -H "Connection: keep-alive" \
    -H "Pragma: no-cache" \
    -H "Cache-Control: no-cache" \
    --data "7|0|4|http://${ip}/denali/|${permutation}|org.eclipse.kura.web.shared.service.GwtSecurityTokenService|generateSecurityToken|1|2|3|4|0|" \
    -sS \
    http://${ip}/denali/xsrf | gunzip -c -)
echo ${token:4}