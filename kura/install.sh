#!/bin/bash -x
. ./constants.sh

token=$(./token.sh)

token=$(echo ${token} | awk -F "," '{print $(NF-2)}' | awk -F "\"" '{print $2}')
curl -v \
    -b cookies.txt \
    -c cookies.txt \
    -F uploadedFile=@$1 \
    -F xsrfToken=${token} \
    -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0" \
    -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" \
    -H "Accept-Language: en-US,en;q=0.5" \
    -H "Accept-Encoding: gzip, deflate" \
    -H "Referer: http://${ip}/kura" \
    -H "Authorization: Basic YWRtaW46YWRtaW4=" \
    -H "Connection: keep-alive" \
    -H "Upgrade-Insecure-Requests: 1" \
    -H "Pragma: no-cache" \
    -H "Cache-Control: no-cache" \
    -H "Host: ${ip}" \
    http://${ip}/denali/file/deploy/upload