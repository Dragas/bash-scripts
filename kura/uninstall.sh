#!/bin/bash -x

. ./constants.sh

token=$(./token.sh)
xsrf=$(echo ${token} | awk -F "," '{print $(NF-2)}' | awk -F "\"" '{print $2}')
bundle=$1
checksum=$(echo ${token} | awk -F "'" '{print $2}')

data="7|0|9|http://${ip}/denali/|${permutation}|org.eclipse.kura.web.shared.service.GwtPackageService|uninstallDeploymentPackage|org.eclipse.kura.web.shared.model.GwtXSRFToken/1747849864|java.lang.String/2004016611|java.util.Date/3385151746|${xsrf}|${bundle}|1|2|3|4|2|5|6|5|7|${checksum}|8|9|"
curl -v \
    -b cookies.txt \
    -c cookies.txt \
    --data "${data}" \
    -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0" \
    -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" \
    -H "Accept-Language: en-US,en;q=0.5" \
    -H "Accept-Encoding: gzip, deflate" \
    -H "Referer: http://${ip}/kura" \
    -H "Authorization: Basic YWRtaW46YWRtaW4=" \
    -H "Connection: keep-alive" \
    -H "Upgrade-Insecure-Requests: 1" \
    -H "Pragma: no-cache" \
    -H "Cache-Control: no-cache" \
    -H "Host: ${ip}" \
    -H "Content-Type: text/x-gwt-rpc; charset=utf-8" \
    -H "X-GWT-Permutation: ${permutation}" \
    -H "X-GWT-Module-Base: http://${ip}/denali/" \
    http://${ip}/denali/package | gunzip -c