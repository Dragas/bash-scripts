# Kura Scripts

## Requirements

* Kura runtime available somewhere (Enter the ip address in constants.sh).
* Maven Wrapper.
* Every now and then permutation changes, so you have to change it accordingly in constants.sh
  * To find a new one, check network tab while accessing packages tab in kura in XSRF request, `X-GWT-PERMUTATION` permutation header.
* These scripts should be placed at top level of Kura project

## Usage

```
./upload.sh [artifact name]
```

The process is as follows:
* `uninstall.sh` is executed with that artifact's name.
* `clean.sh` is executed.
* An equivalent DP file is found.
* If the DP file exists, its path is passed to `install.sh` which uploads the file to your kura installation.

## Explanations

### `clean.sh`

Removes all target directories from the project, to ensure that "clean" compilation is performed.

Then `./mvnw clean install` is performed to compile entire project.

### `constants.sh`

Holds constants for other scripts.

### `install.sh`

```
./install.sh [dp path]
```

Uploads provided deployment package to your kura installation.

### `token.sh`

Parses the XSRF token from your kura installation.

Used by install and uninstall scripts.

### `uninstall.sh`

```
./uninstall.sh [artifact name]
```

Removes particular artifact from Kura installation.

### `upload.sh`

```
./upload.sh [artifact name]`
```

Calls the pipeline provided at `Usage`